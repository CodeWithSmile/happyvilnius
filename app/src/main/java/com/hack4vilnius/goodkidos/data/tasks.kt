package com.hack4vilnius.goodkidos.data

import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import com.hack4vilnius.goodkidos.R


data class Task(val name: String, val description: String, val image: Int, val fluffy: Int, val points: String, val paid: Int)


val tasksData = arrayOf(
    Task("Nusivalyti telefono ekraną", "Švelnutis ekraną mėgsta valyti liežuviu, kadangi neturi servetėles. Padėk Švelnučiui ir nusivalyk telefoną servetėle.",  R.drawable.tel, R.drawable.svelnutis, "100", 0),
    Task("Neverk dėl prarasto kraujo", "",   R.drawable.tel, R.drawable.svelnutis, "100", 0),
    Task("Surasti Vytautą, Gediminą ir Mindaugą", "",  R.drawable.pukutis_mindaugas, R.drawable.pukutis, "500", 0),
    Task("Nukeliauti į praeitį", "",  R.drawable.pukutis_mindaugas, R.drawable.pukutis, "700", 0),
    Task("Nueiti į Mažvydo, arba artimiausią biblioteką", "",  R.drawable.pukutis_mindaugas, R.drawable.slidutis, "1000", R.drawable.coin),
    Task("Pasidalinti skanumynais su draugu", "",  R.drawable.pukutis_mindaugas, R.drawable.slidutis, "1500", R.drawable.coin),
    Task("Nusifotografuoti su prezidentu", "",  R.drawable.pukutis_mindaugas, R.drawable.slidutis, "10000",R.drawable.coin)

)
