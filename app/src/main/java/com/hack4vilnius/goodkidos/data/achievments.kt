package com.hack4vilnius.goodkidos.data

import com.hack4vilnius.goodkidos.R

data class Achievment(val name: String, val image: Int, val unlocked: Boolean)

val achievmentsData = arrayOf(
    Achievment("Nusileidimas sėkmingas",  R.drawable.landing, true),
    Achievment("Bannana!!!", R.drawable.banan, true),
    Achievment("Gilė prie gilės ir ažuoliukas", R.drawable.gile,true),
    Achievment("Tavo draugas - mano draugas", 0, false),
    Achievment("Ai! Skauda!", 0, false),
    Achievment("Piktas kaip Keksas?", 0, false),
    Achievment("Aš programuotojas", 0, false),
    Achievment("Tapetų menas", 0, false)

    )