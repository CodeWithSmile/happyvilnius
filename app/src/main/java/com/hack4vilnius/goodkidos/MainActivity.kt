package com.hack4vilnius.goodkidos

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import android.view.LayoutInflater
import android.view.View
import com.hack4vilnius.goodkidos.dummy.DummyContent
import kotlinx.android.synthetic.main.custom_action_bar.*
import com.hack4vilnius.goodkidos.data.tasksData
import com.hack4vilnius.goodkidos.data.achievmentsData
import java.util.*
import kotlin.concurrent.schedule
import android.widget.TextView




class MainActivity : AppCompatActivity(), AchievementFragment.OnAchievementListFragmentInteractionListener,
    TaskFragment.OnTaskPointListener {

    private val tasks = tasksData
    private val achievements = achievmentsData

    var userName: String? = null
    var experience: Int = 0
    var level: Int = 0

    override fun onTaskPoint(points: Int) {
        runOnUiThread {
            experience += points

            if (experience % 1000 == 0) {
                level += 1
                text_points.text = experience.toString()

                val intent = Intent(this, FullscreenActivity::class.java)
                intent.putExtra("userName", userName)
                intent.putExtra("experience", experience)
                startActivity(intent)

                }
            }
        }



    override fun onAchievementListFragmentInteraction(item: DummyContent.DummyItem?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_tasks -> {
                    val fragment = TaskFragment.newInstance(1)
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.main_container, fragment)
                        .commit()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_achievements -> {
                    val fragment = AchievementFragment.newInstance(1)
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.main_container, fragment)
                        .commit()
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        userName = intent.getStringExtra("userName")
        experience = intent.getIntExtra("experience", 0)
        super.onCreate(savedInstanceState)


        setContentView(R.layout.activity_main)
        setSupportActionBar(detail_toolbar)

        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayShowHomeEnabled(false)

        val mInflater = LayoutInflater.from(this)
        val customView = mInflater.inflate(R.layout.custom_action_bar, null)
        supportActionBar!!.customView = customView
        supportActionBar!!.setDisplayShowCustomEnabled(true)

        val fragment = TaskFragment.newInstance(1)
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.main_container, fragment)
            .commit()
        navigation.itemIconTintList = null
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    override fun onResume() {
        super.onResume()
        title_text.text = userName
        text_points.text = experience.toString()
    }
}
