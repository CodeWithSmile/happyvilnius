package com.hack4vilnius.goodkidos

import android.support.v7.widget.RecyclerView
import android.text.Layout
import android.transition.Visibility
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView


import com.hack4vilnius.goodkidos.data.Task
import com.hack4vilnius.goodkidos.dummy.DummyContent.DummyItem

import kotlinx.android.synthetic.main.fragment_task.view.*
import kotlinx.android.synthetic.main.task_content_view.view.*
import kotlinx.android.synthetic.main.task_header_view.view.*
import java.util.*
import kotlin.concurrent.schedule
import android.widget.LinearLayout
import android.widget.RelativeLayout


/**
 * [RecyclerView.Adapter] that can display a [DummyItem] and makes a call to the
 * specified [OnTaskListFragmentInteractionListener].
 * TODO: Replace the implementation with code for your data type.
 */
class MyTaskRecyclerViewAdapter(
    private val mValues: Array<Task>,
    private val mListenerTask: TaskFragment.OnTaskPointListener?
) : RecyclerView.Adapter<MyTaskRecyclerViewAdapter.ViewHolder>() {

//    private val mOnClickListener: View.OnClickListener

//    init {
//        mOnClickListener = View.OnClickListener { v ->
//            val item = v.tag as DummyItem
//            // Notify the active callbacks interface (the activity, if the fragment is attached to
//            // one) that an item has been selected.
//            mListenerTask?.onTaskListFragmentInteraction(item)
//        }
//    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_task, parent, false)
        return ViewHolder(view)
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.mHeader.taskHeaderText.text = item.name
        holder.mContentView.taskImage.setImageResource(item.image)
        holder.mHeader.fluffyImage.setImageResource(item.fluffy)
        if (item.paid == 0){
            holder.mHeader.itemPointText.text = item.points
            holder.mHeader.taskHeaderText.isEnabled = true
            holder.mHeader.coinImg.visibility = View.GONE

        }
        else {
            holder.mHeader.itemPointText.text = ""
            holder.mHeader.leafImage.visibility = View.GONE
            holder.mHeader.taskHeaderText.isEnabled = false
            holder.mHeader.coinImg.setImageResource(item.paid)

        }

//        with(holder.mView) {
//            tag = item
//            setOnClickListener(mOnClickListener)
//        }

        holder.mContentView.taskStartBtn.setOnClickListener {
            //            holder.mExpandable.setBackgroundColor()
            holder.mContentView.taskCancelBtn.visibility = View.VISIBLE
            holder.mContentView.taskFinishBtn.visibility = View.VISIBLE
            holder.mContentView.taskStartBtn.visibility = View.GONE
        }

        holder.mContentView.taskCancelBtn.setOnClickListener {
            //            holder.mExpandable.setBackgroundColor()
            holder.mContentView.taskCancelBtn.visibility = View.GONE
            holder.mContentView.taskFinishBtn.visibility = View.GONE
            holder.mContentView.taskStartBtn.visibility = View.VISIBLE
        }

        holder.mContentView.taskFinishBtn.setOnClickListener {
            //            holder.mExpandable.setBackgroundColor()
            holder.mContentView.taskCancelBtn.visibility = View.GONE
            holder.mContentView.taskFinishBtn.visibility = View.GONE
            holder.mContentView.taskStartBtn.visibility = View.GONE
            Timer("WaitingForApproval", false).schedule(5000) {
                // send real points
                mListenerTask!!.onTaskPoint(100)
            }
        }

        holder.mExpandable.requestLayout();
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mExpandable: View = mView.expandable
        val mHeader: View = mView.header
        val mContentView: View = mView.content

        override fun toString(): String {
            return super.toString()// + " '" + mContentView.text + "'"
        }
    }
}
