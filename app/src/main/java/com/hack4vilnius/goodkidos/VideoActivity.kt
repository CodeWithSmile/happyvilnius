package com.hack4vilnius.goodkidos

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_video.*

class VideoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_video)
    }

    override fun onResume() {
        super.onResume()
        skip.setOnClickListener {
            startNextActivity()
        }

        videoView.setOnCompletionListener {
            startNextActivity()
        }

        val uri = Uri.parse("android.resource://" + packageName + "/" + R.raw.videogame)
        videoView.setVideoURI(uri)
        videoView.requestFocus()
        videoView.start()
    }

    private fun startNextActivity() {
        val intent = Intent(this, NameActivity::class.java)
        startActivity(intent)
    }
}
